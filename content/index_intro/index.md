---
headless: true
date: 2021-04-17
---

LFortran is a modern open-source (BSD licensed) interactive Fortran compiler
built on top of LLVM. It can execute user's code interactively to allow
exploratory work (much like Python, MATLAB or Julia) as well as compile to
binaries with the goal to run user's code on modern architectures such as
multi-core CPUs and GPUs.

LFortran is in development (pre-alpha stage), see below for the current
status and a roadmap.

**News March 9, 2021**: LFortran is participating in Google Summer of Code 2021!
Please see the
[ideas](https://gitlab.com/lfortran/lfortran/-/wikis/GSoC-2021-Ideas) page for
more information about project ideas and how to apply.

Main repository at GitLab:
[https://gitlab.com/lfortran/lfortran](https://gitlab.com/lfortran/lfortran)
[![GitLab Stars](https://img.shields.io/badge/dynamic/json?color=white&label=Stars&query=%24.star_count&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F4494718)](https://gitlab.com/lfortran/lfortran/-/starrers)  
GitHub mirror:
[https://github.com/lfortran/lfortran](https://github.com/lfortran/lfortran)
{{< github_lfortran_button >}}  
Try online using Binder:
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/lfortran%2Fweb%2Flfortran-binder/master?filepath=Demo.ipynb)  
Any questions? Ask us on Zulip [![project chat](https://img.shields.io/badge/zulip-join_chat-brightgreen.svg)](https://lfortran.zulipchat.com/)
or our [mailing list](https://groups.io/g/lfortran). Follow us on Twitter to get
the latest updates: [@lfortranorg](https://twitter.com/lfortranorg).
