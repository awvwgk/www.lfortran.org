# Feature Highlights

LFortran is in development, there are features that work today, and there are
features that are being implemented.

## Works today

* **Interactive, Jupyter support**  
    LFortran can be used as a Jupyter kernel, allowing Python/Julia-style rapid
    prototyping and an exploratory workflow (see the [static] or [interactive]
    example notebook). It can also be used from the command-line with an
    interactive prompt (REPL).

* **Clean, modular design, usable as a library**  
    LFortran is structured around two independent modules, AST and ASR, both of
    which are standalone (completely independent of the rest of LFortran) and
    users are encouraged to use them independently for other applications and
    build tools on top. See the [Design](https://docs.lfortran.org/design/)
    and [Developer Tutorial](https://docs.lfortran.org/developer_tutorial/)
    documents for more details.

* **Initial interoperation with GFortran**  
    LFortran has a prototype parser for GFortran module files and generation of
    a Fortran wrapper that allows using the module with any Fortran compiler.
    This feature is still under development.

* **Create executables**  
    It can create executables just like other Fortran compilers.

* **Runs on Linux, Mac and Windows**  
    All three platforms are regularly tested by our CI.


## Planned

These features are under development, there is a link to the corresponding
issue so that you can track the progress by following it.

* **Native interoperation with other languages (and other Fortran compilers)**  
    Import libraries written in other languages (such as C or Python) with the
    `use` statement, see [#44]. Add support for module files generated with
    other Fortran compilers, see [#56]. This will allow LFortran to run a large
    array of production codes.

* **Support for diverse hardware**  
    LLVM makes it possible to run LFortran on diverse hardware and take
    advantage of native Fortran language constructs (such as `do concurrent`)
    on multi-core CPUs and GPUs, see [#57].

* **Full Fortran 2018 support**  
    Currently only a subset of Fortran is implemented (we plan to document
    exactly what is supported [#92], in the meantime it is just a bit more than
    what is presented in the [notebook][static]), but the goal is to have a
    full implementation of the latest Fortran 2018 standard, see [#58].


Please vote on issues in our [issue tracker] that you want us to prioritize
(feel free to create new ones if we are missing anything).

# Roadmap

The tasks listed here provide early users with the opportunity to contribute
bug reports, feature requests and code. This way LFortran will gradually become
useful to more people and the community will grow.

1. Get GFortran module files working with `use` module, both assumed-size and
   assumed-shape arrays ([#52]). This will make LFortran usable for early
   users. We expect to finish this task by the end of summer 2019.

2.  

    + Finish the implementation of the Fortran language, including the 2018
      standard ([#58]).

    + Gradually rewrite the compiler in C++ for robustness and speed ([#70]).
      The Python API will not change (or only minimally).

    + Implement all the other cool features:
        * `use` libraries written in C and Python with automatic wrappers ([#44])
        * Support for diverse hardware ([#57])
        * Convert code to older Fortran standards ([#72])
        * Fortran doctest support ([#73])
        * SymPy integration ([#71])
        * Language service for IDEs ([#12])
        * Other ideas ([#29])


[static]: https://nbviewer.jupyter.org/gist/certik/f1d28a486510810d824869ab0c491b1c
[interactive]: https://mybinder.org/v2/gl/lfortran%2Fweb%2Flfortran-binder/master?filepath=Demo.ipynb

[issue tracker]: https://gitlab.com/lfortran/lfortran/issues
[#12]: https://gitlab.com/lfortran/lfortran/issues/12 'Implement "Language Service"'
[#29]: https://gitlab.com/lfortran/lfortran/issues/29 "Potential features"
[#44]: https://gitlab.com/lfortran/lfortran/issues/44 "Bridge to Python and other languages"
[#52]: https://gitlab.com/lfortran/lfortran/issues/52 'Allow to "use" modules compiled by gfortran'
[#56]: https://gitlab.com/lfortran/lfortran/issues/56 "Add support for other Fortran compilers"
[#57]: https://gitlab.com/lfortran/lfortran/issues/57 "Modern architectures support"
[#58]: https://gitlab.com/lfortran/lfortran/issues/58 "Support the latest Fortran 2018 standard"
[#70]: https://gitlab.com/lfortran/lfortran/issues/70 "Port to C++"
[#71]: https://gitlab.com/lfortran/lfortran/issues/71 "SymPy integration"
[#72]: https://gitlab.com/lfortran/lfortran/issues/72 "Rewrite code to an older Fortran standard"
[#73]: https://gitlab.com/lfortran/lfortran/issues/73 "Implement Fortran doctest feature"
[#74]: https://gitlab.com/lfortran/lfortran/issues/74 "Port all of LFortran to use ASR"
[#92]: https://gitlab.com/lfortran/lfortran/issues/92 "Document what subset of Fortran is currently supported"
